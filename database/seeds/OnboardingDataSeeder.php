<?php

use Illuminate\Database\Seeder;

class OnboardingDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = storage_path('app/export.csv');
        // opening the csv file
        if (($handle = fopen($csvFile, "r")) !== false) {
            // looping through the entire file
            while (!feof($handle)) {
                // assigning into array
                $outputData[] = fgetcsv($handle, 1000, ';');
            }
            // closing the open file connection
            fclose($handle);
        }

        foreach ($outputData as $key => $value) {
            if ($key>0 && $value[1] != null) {
                $seedData[] = array("user_id" => $value[0], "onboarded_at" => $value[1], "percentage" => $value[2]);
            }
        }

        DB::table('onboarding')->insert($seedData);
    }
}
