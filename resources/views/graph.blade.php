<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" >

        <title>Temper</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="content" id="app">
            <!-- Vue.js codes -->
            <!-- <graph></graph>  --> 
            <div id="container" style="width:100%; height:400px;"></div>
        </div>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="{{ asset('js/graph.js') }}"></script>       
    </body>
</html>
