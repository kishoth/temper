<?php
namespace App;

use App\Models\Onboarding as Onboarding;

class Chart
{
    const APPROVAL = 100;
    const WAIT_APPROVAL = 99;
    const FREELANCER = 90;
    const EXPERIENCE = 70;
    const INTEREST = 50;
    const PROFILE = 40;
    const ACTIVATE = 20;

    /**
     * Preparing chart data for the on boarding flow within given date range
     * @param string $startDate
     * @param string $endDate
     * @return array
     */
    public function onboardingChartData($startDate, $endDate)
    {
        $total = Onboarding::dateRange($startDate, $endDate);
        $approval = Onboarding::dateRange($startDate, $endDate)->percentage(self::APPROVAL);
        $waitApproval = Onboarding::dateRange($startDate, $endDate)->percentageRange(self::WAIT_APPROVAL, self::APPROVAL);
        $freelancer = Onboarding::dateRange($startDate, $endDate)->percentageRange(self::FREELANCER, self::WAIT_APPROVAL);
        $experience = Onboarding::dateRange($startDate, $endDate)->percentageRange(self::EXPERIENCE, self::FREELANCER);
        $interest = Onboarding::dateRange($startDate, $endDate)->percentageRange(self::INTEREST, self::EXPERIENCE);
        $profile = Onboarding::dateRange($startDate, $endDate)->percentageRange(self::PROFILE, self::INTEREST);
        $activate = Onboarding::dateRange($startDate, $endDate)->percentageRange(self::ACTIVATE, self::PROFILE);
        $create = Onboarding::dateRange($startDate, $endDate)->percentageBelowRange(self::ACTIVATE);

        $chartDataCollection = collect([$total, $create, $activate, $profile, $interest, $experience, $freelancer, $waitApproval, $approval]);

        return $chartDataCollection;
    }
}
