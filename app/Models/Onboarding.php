<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Onboarding extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'onboarding';

    /**
     * Query between a date range
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $startDate
     * @param string $endDate
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDateRange($query, $startDate, $endDate)
    {
        return $query->where('onboarded_at', '>=', $startDate)->where('onboarded_at', '<=', $endDate);
    }

    /**
     * Query for a percentage
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $percentage
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePercentage($query, $percentage)
    {
        return $query->where('percentage', '=', $percentage);
    }

    /**
     * Query for a percentage range
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $lessVal
     * @param integer $highVal
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePercentageRange($query, $lessVal, $highVal)
    {
        return $query->where('percentage', '>=', $lessVal)->where('percentage', '<', $highVal);
    }

    /**
     * Query to get data below the given range
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param integer $highVal
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePercentageBelowRange($query, $highVal)
    {
        return $query->where('percentage', '<', $highVal);
    }

}
