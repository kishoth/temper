<?php

namespace App\Outputs;

use App\Helpers\CalculatePercentage as CalculatePercentage;

/**
 * This class is responsible to create chart related output data
 * Class ChartOutput
 * @package App\Outputs
 * @author Kishoth <kishoth@gapstars.net>
 */
class ChartOutput
{
    protected $calculatePercentage;

    public function __construct(CalculatePercentage $calculatePercentage)
    {
        $this->calculatePercentage = $calculatePercentage;
    }

    /**
     * Setting up the output with percentage of users in each stage
     * @param Illuminate\Support\Collection $chartData
     * @return string
     */
    public function retentionCurveWeeklyCohort($chartData)
    {
        $onboardingStepCount = $chartData->map(function ($item) {
            return $item->count();
        });

        $totalCount = $onboardingStepCount->shift();

        $onboardingPercentage = $onboardingStepCount->map(function ($item) use ($totalCount) {
            return $this->calculatePercentage->calculate($item, $totalCount);
        });

        return $onboardingPercentage->toJson();
    }

}