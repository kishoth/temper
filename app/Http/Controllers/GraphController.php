<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chart;
use App\Outputs\ChartOutput;

class GraphController extends Controller
{
    protected $chart;
    protected $chartOutput;

    public function __construct(Chart $chart, ChartOutput $chartOutput)
    {
        $this->chart = $chart;
        $this->chartOutput = $chartOutput;
    }

    public function chartWeeklyData($startDate, $endDate)
    {
        $chartData = $this->chart->onboardingChartData($startDate, $endDate);

        $chartOutputData = $this->chartOutput->retentionCurveWeeklyCohort($chartData);

        return $chartOutputData;
    }
}
