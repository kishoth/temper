<?php
namespace App\Helpers;

/**
 * Class CalculatePercentage
 * @package App\Helpers
 * @author Kishoth <kishoth@gapstars.net>
 */
class CalculatePercentage
{
    /**
     * To calculate percentage for a value($val) compared to the total($tot)
     * @param integer $value
     * @param integer $total
     * @return float
     */
    public function calculate($value, $total)
    {
        // handling division by zero exception
        if ($total == 0) :
            $formatValue = 0;
        else :
            $percentage = ($value/$total)*100;
            // formatting for 2 decimals
            $formatValue = number_format($percentage, 2);
        endif;

        // casting to float
        return (float) $formatValue;
    }
}
