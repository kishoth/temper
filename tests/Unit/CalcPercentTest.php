<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CalcPercentTest extends TestCase
{
    protected $calcPercent;

    public function setup()
    {
        $this->calcPercent = new \App\Helpers\CalculatePercentage;
    }

    /**
    @test
     */
    public function calculate_percentage_correctly()
    {
        $this->assertEquals(50, $this->calcPercent->calculate(5, 10));
    }

    /**
    @test
     */
    public function handle_divide_by_zero()
    {
        $this->assertEquals(0, $this->calcPercent->calculate(10, 0));
    }
}
