<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Onboarding as Onboarding;

class ChartTest extends TestCase
{
    protected $chart;

    public function setup()
    {
        parent::setUp();

        $this->chart = new \App\Chart();
    }

    /**
    @test
     */
    public function outputs_object()
    {
        $this->assertInternalType('object', $this->chart->onboardingChartData("2017-09-01", "2017-09-08"));
    }

    /**
    @test
     */
    public function outputs_object_for_wrong_inputs()
    {
       $this->assertInternalType('object', $this->chart->onboardingChartData("string1", "string2"));
    }

    /**
     @test
     */
    public function outputs_correct_number_of_elements()
    {
        $this->assertCount(9, $this->chart->onboardingChartData("2017-09-01", "2017-09-08"));
    }

    /**
     @test
     */
    public function contains_only_value_of_collection()
    {
        $this->assertContainsOnly('object', $this->chart->onboardingChartData("2017-09-01", "2017-09-08"));
    }
}
