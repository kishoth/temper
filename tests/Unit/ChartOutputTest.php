<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ChartOutputTest extends TestCase
{
    protected $calculate;
    protected $chartOutput;
    protected $collection;

    public function setup()
    {
        parent::setUp();

        $this->calculate = new \App\Helpers\CalculatePercentage;
        $this->chartOutput = new \App\Outputs\ChartOutput($this->calculate);
        $this->collection = collect([new \App\Models\Onboarding(), new \App\Models\Onboarding(), new \App\Models\Onboarding()]);
    }

    /**
    @test
     */
    public function calculate_percentage_correctly()
    {
        $this->assertInternalType('string', $this->chartOutput->retentionCurveWeeklyCohort($this->collection));
    }
}
