// codes for making http request
var http = new XMLHttpRequest();
var http2 = new XMLHttpRequest();
var http3 = new XMLHttpRequest();
var http4 = new XMLHttpRequest();

let graphData = [];
http.onreadystatechange = function()
{
    if(this.readyState == 4 && this.status == 200) {
        var responseText = this.responseText;

        // setting up the response to javascript object
        let jsonResponse = JSON.parse(responseText);
       graphData[0] = jsonResponse;
    }
};

http2.onreadystatechange = function()
{
    if(this.readyState == 4 && this.status == 200) {
        var responseText = this.responseText;

        // setting up the response to javascript object
        let jsonResponse = JSON.parse(responseText);
        graphData[1] = jsonResponse;
    }
};

http3.onreadystatechange = function()
{
    if(this.readyState == 4 && this.status == 200) {
        var responseText = this.responseText;

        // setting up the response to javascript object
        let jsonResponse = JSON.parse(responseText);
        graphData[2] = jsonResponse;
    }
};


http4.onreadystatechange = function(jsonResponse2)
{
	if(this.readyState == 4 && this.status == 200)
	{
		var responseText = this.responseText;

		// setting up the response to javascript object
		var jsonResponse = JSON.parse(responseText);
		graphData[3] = jsonResponse;

		var xLabels = ["0%", "20%", "40%", "50%", "70%", "90%", "99%", "100%"];

		// codes for the chart
		var myChart = Highcharts.chart('container', {
				chart: {
					type: 'spline'
				},
				title: {
					text: 'Weekly Retention Curves'
				},
				xAxis: {
					labels:{
						formatter: function() {
							return xLabels[this.value];
						}
					}
				},
				yAxis: {
					title: {
						text: 'Percentage of users'
					},
					max: 100
				},

				series: [{
					name: 'Week 0',
					data: [100,0,0,0,0,0,0,0]
				}, {
					name: 'Week 1',
					data: graphData[0]
				}, {
					name: 'Week 2',
					data: graphData[1]
				}, {
					name: 'Week 3',
					data: graphData[2]
				}, {
					name: 'Week 4',
					data: graphData[3]
				}]
		});
	}
};

http.open("GET", "/api/chart-weekly-data/start/2016-07-19/end/2016-07-25");
http2.open("GET", "/api/chart-weekly-data/start/2016-07-26/end/2016-08-01");
http3.open("GET", "/api/chart-weekly-data/start/2016-08-02/end/2016-08-08");
http4.open("GET", "/api/chart-weekly-data/start/2016-08-09/end/2016-08-15");

http.send();
http2.send();
http3.send();
http4.send();


